/* Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#define CONFIG_SND_SOC_IPQ60XX 1
#define CONFIG_SND_SOC_QDSP6V2 1
#define CONFIG_CODEC_CORE 1
#define CONFIG_SND_SOC_WSA881X 1
#define CONFIG_SND_SOC_BOLERO 1
#define CONFIG_TX_MACRO 1
#define CONFIG_WSA_MACRO 1
#define CONFIG_VA_MACRO 1
#define CONFIG_AUDIO_PKT 1
#define CONFIG_GECKO_CORE 1
#define CONFIG_AUDIO_PRM 1
#define CONFIG_MSM_ADSP_LOADER 1
#define CONFIG_MSM_QDSP6_NOTIFIER 1
#define CONFIG_MSM_QDSP6_GPR 1
#define CONFIG_SOUNDWIRE 1
#define CONFIG_SOUNDWIRE_MSTR_CTRL 1
#define CONFIG_MSM_QDSP6_SSR 1
#define CONFIG_SND_SOC_MSM_STUB 1
#define CONFIG_SND_SOC_MSM_QDSP6V2_INTF 1

