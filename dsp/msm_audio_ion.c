/*
 * Copyright (c) 2013-2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/list.h>
#include <linux/dma-mapping.h>
#include <linux/dma-buf.h>
#include <linux/iommu.h>
#include <linux/platform_device.h>
#include <linux/of_device.h>
#include <linux/export.h>
#include <linux/ioctl.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/msm_audio.h>
#include <dsp/msm_audio_ion.h>

#define MSM_AUDIO_ION_PROBED (1 << 0)
#define MSM_AUDIO_ION_CLIENT "audio_client"
#define MSM_AUDIO_ION_PHYS_ADDR(alloc_data) \
	alloc_data->table->sgl->dma_address

#define MSM_AUDIO_ION_VA_START 0x10000000
#define MSM_AUDIO_ION_VA_LEN 0x0FFFFFFF

#define MSM_AUDIO_SMMU_SID_OFFSET 32

#define MSM_AUDIO_ION_DRIVER_NAME "msm_audio_ion"
#define MINOR_NUMBER_COUNT 1
struct msm_audio_ion_private {
	bool smmu_enabled;
	struct device *cb_dev;
	struct dma_iommu_mapping *mapping;
	u8 device_status;
	struct list_head alloc_list;
	struct mutex list_mutex;
	u64 smmu_sid_bits;
	u32 smmu_version;
	/*list to store fd,phy addr and handle data*/
	struct list_head fd_list;
	/*Character device details*/
	dev_t ion_major;
	struct class *ion_class;
	struct device *chardev;
	struct cdev dev;
};
static struct msm_audio_ion_private msm_audio_ion_data = {0,};
struct msm_audio_alloc_data {
	struct ion_client *client;
	struct ion_handle *handle;
	size_t len;
	struct dma_buf *dma_buf;
	struct dma_buf_attachment *attach;
	struct sg_table *table;
	struct list_head list;
};

struct msm_audio_fd_data {
	int fd;
	struct ion_handle *handle;
	int64_t paddr;
	struct list_head list;
};

static void msm_audio_ion_add_allocation(
	struct msm_audio_ion_private *msm_audio_ion_data,
	struct msm_audio_alloc_data *alloc_data)
{
	/*
	 * Since these APIs can be invoked by multiple
	 * clients, there is need to make sure the list
	 * of allocations is always protected
	 */
	mutex_lock(&(msm_audio_ion_data->list_mutex));
	list_add_tail(&(alloc_data->list),
		      &(msm_audio_ion_data->alloc_list));
	mutex_unlock(&(msm_audio_ion_data->list_mutex));
}

static int msm_audio_dma_buf_map(struct ion_client *client,
		struct ion_handle *handle,
		u64 *addr, size_t *len)
{
	struct msm_audio_alloc_data *alloc_data;
	struct device *cb_dev;
	int rc = 0;

	cb_dev = msm_audio_ion_data.cb_dev;
	/* Data required per buffer mapping */
	alloc_data = kzalloc(sizeof(*alloc_data), GFP_KERNEL);
	if (!alloc_data)
		return -ENOMEM;

	/* Get the ION handle size */
	ion_handle_get_size(client, handle, len);

	alloc_data->client = client;
	alloc_data->handle = handle;
	alloc_data->len = *len;

	rc = dma_set_max_seg_size(cb_dev, *len);
	if (rc)
		pr_err("%s:dma set max seg size failed\n", __func__);

	/* Get the dma_buf handle from ion_handle */
	alloc_data->dma_buf = ion_share_dma_buf(client, handle);
	if (IS_ERR(alloc_data->dma_buf)) {
		rc = PTR_ERR(alloc_data->dma_buf);
		dev_err(cb_dev,
			"%s: Fail to get dma_buf handle, rc = %d\n",
			__func__, rc);
		goto err_dma_buf;
	}

	/* Attach the dma_buf to context bank device */
	alloc_data->attach = dma_buf_attach(alloc_data->dma_buf,
					    cb_dev);
	if (IS_ERR(alloc_data->attach)) {
		rc = PTR_ERR(alloc_data->attach);
		dev_err(cb_dev,
			"%s: Fail to attach dma_buf to CB, rc = %d\n",
			__func__, rc);
		goto err_attach;
	}

	/*
	 * Get the scatter-gather list.
	 * There is no info as this is a write buffer or
	 * read buffer, hence the request is bi-directional
	 * to accommodate both read and write mappings.
	 */
	alloc_data->table = dma_buf_map_attachment(alloc_data->attach,
				DMA_BIDIRECTIONAL);
	if (IS_ERR(alloc_data->table)) {
		rc = PTR_ERR(alloc_data->table);
		dev_err(cb_dev,
			"%s: Fail to map attachment, rc = %d\n",
			__func__, rc);
		goto err_map_attach;
	}

	rc = dma_map_sg(cb_dev, alloc_data->table->sgl,
			alloc_data->table->nents,
			DMA_BIDIRECTIONAL);
	if (rc == 0) {
		dev_err(cb_dev,
			"%s: Fail to map SG, rc = %d, nents = %d\n",
			__func__, rc, alloc_data->table->nents);
		goto err_map_sg;
	}
	/* Make sure not to return rc from dma_map_sg, as it can be nonzero */
	rc = 0;

	/* physical address from mapping */
	*addr = MSM_AUDIO_ION_PHYS_ADDR(alloc_data);

	msm_audio_ion_add_allocation(&msm_audio_ion_data,
				     alloc_data);
	return rc;

err_map_sg:
	dma_buf_unmap_attachment(alloc_data->attach,
				 alloc_data->table,
				 DMA_BIDIRECTIONAL);
err_map_attach:
	dma_buf_detach(alloc_data->dma_buf,
		       alloc_data->attach);
err_attach:
	dma_buf_put(alloc_data->dma_buf);

err_dma_buf:
	kfree(alloc_data);

	return rc;
}

static int msm_audio_ion_get_phys(struct ion_client *client,
		struct ion_handle *handle,
		u64 *addr, size_t *len)
{
	int rc = 0;

	pr_debug("%s: smmu_enabled = %d\n", __func__,
		msm_audio_ion_data.smmu_enabled);

	if (msm_audio_ion_data.smmu_enabled) {
		rc = msm_audio_dma_buf_map(client, handle, addr, len);
		if (rc) {
			pr_err("%s: failed to map DMA buf, err = %d\n",
				__func__, rc);
			goto err;
		}
		/* Append the SMMU SID information to the IOVA address */
		*addr |= msm_audio_ion_data.smmu_sid_bits;
	} else {
		rc = ion_phys(client, handle, (ion_phys_addr_t *)addr, len);
	}

err:
	return rc;
}

static struct ion_client *msm_audio_ion_client_create(const char *name)
{
	struct ion_client *pclient = NULL;

	pclient = msm_ion_client_create(name);
	return pclient;
}

static void msm_audio_ion_client_destroy(struct ion_client *client)
{
	pr_debug("%s: client = %pK smmu_enabled = %d\n", __func__,
		client, msm_audio_ion_data.smmu_enabled);

	ion_client_destroy(client);
}
void msm_audio_fd_list_debug(void)
{
	struct msm_audio_fd_data *msm_audio_fd_data = NULL;

	list_for_each_entry(msm_audio_fd_data,
	&(msm_audio_ion_data.fd_list), list) {
		pr_debug("%s fd %d handle %pK phy addr %llu\n", __func__,
			msm_audio_fd_data->fd, msm_audio_fd_data->handle,
			msm_audio_fd_data->paddr);
	}
}

static int  msm_audio_dma_buf_unmap(struct ion_handle *handle)
{
	int rc = 0;
	struct ion_handle *ion_handle = handle;
	struct msm_audio_alloc_data *alloc_data = NULL;
	struct list_head *ptr, *next = NULL;
	struct device *cb_dev;
	cb_dev = msm_audio_ion_data.cb_dev;

	mutex_lock(&(msm_audio_ion_data.list_mutex));
	list_for_each_safe(ptr, next, &(msm_audio_ion_data.alloc_list)) {
		alloc_data = list_entry(ptr, struct msm_audio_alloc_data, list);
		if (alloc_data->handle == ion_handle) {
			ion_unmap_kernel(alloc_data->client,
				alloc_data->handle);
			dma_unmap_sg(cb_dev,
				alloc_data->table->sgl,
				alloc_data->table->nents, DMA_BIDIRECTIONAL);
			dma_buf_unmap_attachment(
				alloc_data->attach,
				alloc_data->table, DMA_BIDIRECTIONAL);
			dma_buf_detach(alloc_data->dma_buf, alloc_data->attach);
			dma_buf_put(alloc_data->dma_buf);
			list_del(&(alloc_data->list));
			ion_free(alloc_data->client, alloc_data->handle);
			msm_audio_ion_client_destroy(alloc_data->client);
			kfree(alloc_data);
			mutex_unlock(&(msm_audio_ion_data.list_mutex));
			return rc;
		}
	}
	mutex_unlock(&(msm_audio_ion_data.list_mutex));
	dev_err(msm_audio_ion_data.cb_dev,
		"%s: cannot find allocation,handle %pK\n", __func__, handle);
	rc = -EINVAL;
	return rc;
}

void msm_audio_fd_list_update(struct msm_audio_fd_data *fd_data)
{
	struct msm_audio_fd_data *fd_data_head = NULL;

	mutex_lock(&(msm_audio_ion_data.list_mutex));
	list_for_each_entry(fd_data_head,
	&(msm_audio_ion_data.fd_list), list) {
		if (fd_data_head->fd == fd_data->fd) {
			pr_err(
				"%s fd already present,not updating list\n",
				__func__);
			mutex_unlock(&(msm_audio_ion_data.list_mutex));
			return;
		}
	}
	list_add_tail(&fd_data->list, &(msm_audio_ion_data.fd_list));
	mutex_unlock(&(msm_audio_ion_data.list_mutex));
}
void msm_audio_fd_entry_delete(struct ion_handle *handle)
{
	struct msm_audio_fd_data *msm_audio_fd_data = NULL;
	struct list_head *ptr, *next = NULL;

	mutex_lock(&(msm_audio_ion_data.list_mutex));
	list_for_each_safe(ptr, next, &(msm_audio_ion_data.fd_list)) {
		msm_audio_fd_data = list_entry(ptr,
		struct msm_audio_fd_data, list);

		if (msm_audio_fd_data->handle == handle) {
			pr_debug(
				"%s deleting handle %pK entry from list\n",
				__func__, handle);
			list_del(&(msm_audio_fd_data->list));
			kfree(msm_audio_fd_data);
			break;
		}
	}
	mutex_unlock(&(msm_audio_ion_data.list_mutex));
}

static void msm_audio_ion_get_handle(int fd, struct ion_handle **handle)
{
	struct msm_audio_fd_data *fd_data = NULL;

	mutex_lock(&(msm_audio_ion_data.list_mutex));
	list_for_each_entry(fd_data, &(msm_audio_ion_data.fd_list), list) {
		if (fd_data->fd == fd) {
			*handle = fd_data->handle;
			break;
		}
	}
	mutex_unlock(&(msm_audio_ion_data.list_mutex));
}

/**
 * msm_audio_ion_map-
 *        Import ION buffer with given file descriptor
 *
 * @name: name of the client
 * @client: client handle
 * @handle: ion handle
 * @fd: file descriptor for the ION memory
 * @ionflag: flags associated with ION buffer
 * @bufsz: buffer size
 * @paddr: Physical address to be assigned with allocated region
 * @plen: length of allocated region to be assigned
 * @vaddr: virtual address to be assigned
 *
 * Returns 0 on success or error on failure
 */
int msm_audio_ion_map(const char *name, struct ion_client **client,
			struct ion_handle **handle, int fd,
			unsigned long *ionflag, size_t bufsz,
			u64 *paddr, size_t *pa_len, void **vaddr)
{
	int rc = 0;

	if (!(msm_audio_ion_data.device_status & MSM_AUDIO_ION_PROBED)) {
		pr_debug("%s: probe is not done, deferred\n", __func__);
		return -EPROBE_DEFER;
	}

	if (!name || !client || !handle || !paddr || !vaddr || !pa_len) {
		pr_err("%s: Invalid params\n", __func__);
		return -EINVAL;
	}

	*client = msm_audio_ion_client_create(name);
	if (IS_ERR_OR_NULL((void *)(*client))) {
		pr_err("%s: ION create client for AUDIO failed\n", __func__);
		return -EINVAL;
	}

	/* name should be audio_acdb_client or Audio_Dec_Client,
	 *bufsz should be 0 and fd shouldn't be 0 as of now
	 */
	*handle = ion_import_dma_buf(*client, fd);
	pr_debug("%s: DMA buf name=%s, fd=%d handle=%pK\n",
		__func__, name, fd, *handle);
	if (IS_ERR_OR_NULL((void *) (*handle))) {
		pr_err("%s: ion import dma buffer failed\n",
			__func__);
		rc = -EINVAL;
		goto err_destroy_client;
	}

	if (ionflag != NULL) {
		rc = ion_handle_get_flags(*client, *handle, ionflag);
		if (rc) {
			pr_err("%s: could not get flags for the handle\n",
				__func__);
			goto err_ion_handle;
		}
	}

	rc = msm_audio_ion_get_phys(*client, *handle, paddr, pa_len);
	if (rc) {
		pr_err("%s: ION Get Physical for AUDIO failed, rc = %d\n",
			__func__, rc);
		goto err_ion_handle;
	}
	*vaddr = ion_map_kernel(*client, *handle);
	if (IS_ERR_OR_NULL((void *)*vaddr)) {
		pr_err("%s: ION memory mapping for AUDIO failed\n", __func__);
		rc = -ENOMEM;
		goto err_ion_handle;
	}
	pr_debug("%s: mapped address = %pK, size=%zd\n",
		__func__, *vaddr, bufsz);
	return 0;

err_ion_handle:
	ion_free(*client, *handle);
err_destroy_client:
	msm_audio_ion_client_destroy(*client);
	*client = NULL;
	*handle = NULL;

	return rc;
}
EXPORT_SYMBOL(msm_audio_ion_map);

static int msm_audio_smmu_init(struct device *dev)
{
	INIT_LIST_HEAD(&(msm_audio_ion_data.alloc_list));
	INIT_LIST_HEAD(&(msm_audio_ion_data.fd_list));
	mutex_init(&(msm_audio_ion_data.list_mutex));

	return 0;
}

static const struct of_device_id msm_audio_ion_dt_match[] = {
	{ .compatible = "qcom,msm-audio-ion" },
	{ }
};
MODULE_DEVICE_TABLE(of, msm_audio_ion_dt_match);

static int msm_audio_ion_open(struct inode *inode, struct file *file)
{
	struct msm_audio_ion_private *ion_data;
	struct device *dev;

	ion_data = container_of(inode->i_cdev,
		struct msm_audio_ion_private, dev);
	dev = ion_data->chardev;
	file->private_data = ion_data;
	get_device(dev);
	return 0;
}

static int msm_audio_ion_release(struct inode *inode, struct file *file)
{
	struct msm_audio_ion_private *ion_data = container_of(inode->i_cdev,
		struct msm_audio_ion_private, dev);
	struct device *dev = ion_data->chardev;

	put_device(dev);
	return 0;
}

/**
 * msm_audio_get_phy_addr
 *         get physical address for given fd
 *
 * @fd: ION file descriptor
 * @paddr: Physical address corresponding to given fd
 *
 * Returns 0 or success or error on failure
 */
int msm_audio_get_phy_addr(int fd, uint64_t *paddr)
{
	struct msm_audio_fd_data *fd_data = NULL;
	int status = -EINVAL;

	if (!paddr) {
		pr_err("%s Invalid phy addr status %d\n",
			__func__, status);
		return status;
	}
	mutex_lock(&(msm_audio_ion_data.list_mutex));
	list_for_each_entry(fd_data, &(msm_audio_ion_data.fd_list), list) {
		if (fd_data->fd == fd) {
			*paddr = fd_data->paddr;
			status = 0;
			break;
		}
	}
	mutex_unlock(&(msm_audio_ion_data.list_mutex));
	return status;
}
EXPORT_SYMBOL(msm_audio_get_phy_addr);

static int msm_audio_ion_unmap(struct ion_handle *handle)
{
	int ret = -EINVAL;

	if (!handle) {
		pr_err("%s: handle invalid\n", __func__);
		return -EINVAL;
	}
	ret = msm_audio_dma_buf_unmap(handle);
	return ret;
}

/**
 * msm_audio_ion_crash_handler -
 *         handles cleanup after userspace crashes.
 *
 * To be called from machine driver.
 */
void msm_audio_ion_crash_handler(void)
{
	struct msm_audio_fd_data *fd_data = NULL;
	struct list_head *ptr, *next = NULL;
	struct ion_handle *handle = NULL;

	pr_debug("Inside %s\n", __func__);
	list_for_each_entry(fd_data, &(msm_audio_ion_data.fd_list), list) {
		handle = fd_data->handle;
		msm_audio_ion_unmap(handle);
	}
	list_for_each_safe(ptr, next, &(msm_audio_ion_data.fd_list)) {
		fd_data = list_entry(ptr, struct msm_audio_fd_data, list);
		kfree(fd_data);
	}
}
EXPORT_SYMBOL(msm_audio_ion_crash_handler);

/*Ioctl for mapping and unmapping ioctl*/
static long msm_audio_ion_ioctl(struct file *file,
	unsigned int ioctl_num, unsigned long __user ioctl_param)
{
	struct ion_handle *mem_handle = NULL;
	u64 paddr = 0;
	size_t pa_len = 0;
	void *vaddr;
	int ret = 0;
	struct ion_client *client = NULL;
	struct msm_audio_fd_data *msm_audio_fd_data = NULL;

	switch (ioctl_num) {
	case IOCTL_MAP_PHYS_ADDR:
		msm_audio_fd_data = kzalloc(
			sizeof(struct msm_audio_fd_data), GFP_KERNEL);
		if (!msm_audio_fd_data) {
			ret = -ENOMEM;
			pr_err("%s : Out of memory ret %d\n",
				__func__, ret);
		}
		ret = msm_audio_ion_map(
			MSM_AUDIO_ION_CLIENT, &client,
			&mem_handle, (int)ioctl_param,
			NULL, 0, &paddr, &pa_len, &vaddr);
		if (ret < 0) {
			pr_err("%s Memory map failed %d\n",
				__func__, ret);
			kfree(msm_audio_fd_data);
			return ret;
		}
		msm_audio_fd_data->fd = (int)ioctl_param;
		msm_audio_fd_data->handle = mem_handle;
		msm_audio_fd_data->paddr = paddr;
		msm_audio_fd_list_update(msm_audio_fd_data);
		break;
	case IOCTL_UNMAP_PHYS_ADDR:
		msm_audio_ion_get_handle((int)ioctl_param, &mem_handle);
		ret = msm_audio_ion_unmap(mem_handle);
		if (ret < 0) {
			pr_err("%s ion free failed %d\n",
				__func__, ret);
			return ret;
		}
		msm_audio_fd_entry_delete(mem_handle);
		break;
	default:
		pr_err("%s Invalid ioctl number %d\n",
			__func__, ioctl_num);
		ret = -EINVAL;
		break;
	}
	return ret;
}


static const struct file_operations msm_audio_ion_fops = {
	.owner = THIS_MODULE,
	.open = msm_audio_ion_open,
	.release = msm_audio_ion_release,
	.unlocked_ioctl = msm_audio_ion_ioctl,
};

static int msm_audio_ion_reg_chrdev(
	struct msm_audio_ion_private *ion_data)
{
	int ret = 0;

	ret = alloc_chrdev_region(&ion_data->ion_major, 0,
		MINOR_NUMBER_COUNT, MSM_AUDIO_ION_DRIVER_NAME);
	if (ret < 0) {
		pr_err("%s alloc_chrdev_region failed ret : %d\n",
			__func__, ret);
		return ret;
	}
	ion_data->ion_class = class_create(
		THIS_MODULE, MSM_AUDIO_ION_DRIVER_NAME);
	if (IS_ERR(ion_data->ion_class)) {
		ret = PTR_ERR(ion_data->ion_class);
		pr_err("%s class create failed %d\n",
			__func__, ret);
		goto err_class;
	}
	ion_data->chardev = device_create(
		ion_data->ion_class, NULL,
		ion_data->ion_major, NULL,
		MSM_AUDIO_ION_DRIVER_NAME);

	if (IS_ERR(ion_data->chardev)) {
		ret = PTR_ERR(ion_data->ion_class);
		pr_err("%s device create failed %d\n",
			__func__, ret);
		goto err_device;
	}

	cdev_init(&ion_data->dev, &msm_audio_ion_fops);
	ion_data->dev.owner = THIS_MODULE;
	ret = cdev_add(&ion_data->dev,
	ion_data->ion_major, MINOR_NUMBER_COUNT);
	if (ret) {
		pr_err("%s cdev add failed,ret : %d\n",
			__func__, ret);
		goto err_cdev;
	}

	return ret;
err_cdev:
	device_destroy(ion_data->ion_class, ion_data->ion_major);
err_device:
	class_destroy(ion_data->ion_class);
err_class:
	unregister_chrdev_region(
		ion_data->ion_major, MINOR_NUMBER_COUNT);
	return ret;
}

static int msm_audio_ion_unreg_chrdev(
	struct msm_audio_ion_private *ion_data)
{
	cdev_del(&ion_data->dev);
	device_destroy(ion_data->ion_class,
				ion_data->ion_major);
	class_destroy(ion_data->ion_class);
	unregister_chrdev_region(ion_data->ion_major,
						MINOR_NUMBER_COUNT);
	return 0;
}

static int msm_audio_ion_probe(struct platform_device *pdev)
{
	int rc = 0;
	u64 smmu_sid = 0;
	u64 smmu_sid_mask = 0;
	const char *msm_audio_ion_dt = "qcom,smmu-enabled";
	const char *msm_audio_ion_smmu = "qcom,smmu-version";
	const char *msm_audio_ion_smmu_sid_mask = "qcom,smmu-sid-mask";
	bool smmu_enabled;
	struct device *dev = &pdev->dev;
	struct of_phandle_args iommuspec;

	if (dev->of_node == NULL) {
		dev_err(dev,
			"%s: device tree is not found\n",
			__func__);
		msm_audio_ion_data.smmu_enabled = 0;
		return 0;
	}

	smmu_enabled = of_property_read_bool(dev->of_node,
					     msm_audio_ion_dt);
	msm_audio_ion_data.smmu_enabled = smmu_enabled;

	if (!smmu_enabled) {
		dev_dbg(dev, "%s: SMMU is Disabled\n", __func__);
		goto exit;
	}
	dev_dbg(dev, "%s: adsp is ready\n", __func__);

	rc = of_property_read_u32(dev->of_node,
				msm_audio_ion_smmu,
				&msm_audio_ion_data.smmu_version);
	if (rc) {
		dev_err(dev,
			"%s: qcom,smmu_version missing in DT node\n",
			__func__);
		return rc;
	}
	dev_dbg(dev, "%s: SMMU is Enabled. SMMU version is (%d)\n",
		__func__, msm_audio_ion_data.smmu_version);


	/* Get SMMU SID information from Devicetree */
	rc = of_property_read_u64(dev->of_node,
				  msm_audio_ion_smmu_sid_mask,
				  &smmu_sid_mask);
	if (rc) {
		dev_err(dev,
			"%s: qcom,smmu-sid-mask missing in DT node, using default\n",
			__func__);
		smmu_sid_mask = 0xFFFFFFFFFFFFFFFF;
	}

	rc = of_parse_phandle_with_args(dev->of_node, "iommus",
					"#iommu-cells", 0, &iommuspec);
	if (rc)
		dev_err(dev, "%s: could not get smmu SID, ret = %d\n",
			__func__, rc);
	else
		smmu_sid = (iommuspec.args[0] & smmu_sid_mask);

	msm_audio_ion_data.smmu_sid_bits =
		smmu_sid << MSM_AUDIO_SMMU_SID_OFFSET;

	if (msm_audio_ion_data.smmu_version == 0x2) {
		rc = msm_audio_smmu_init(dev);
	} else {
		dev_err(dev, "%s: smmu version invalid %d\n",
			__func__, msm_audio_ion_data.smmu_version);
		rc = -EINVAL;
	}
	if (rc)
		dev_err(dev, "%s: smmu init failed, err = %d\n",
			__func__, rc);

exit:
	if (!rc)
		msm_audio_ion_data.device_status |= MSM_AUDIO_ION_PROBED;

	msm_audio_ion_data.cb_dev = dev;
	if (!dev->dma_parms) {
		dev->dma_parms = devm_kzalloc(dev,
					      sizeof(struct
					      device_dma_parameters),
					      GFP_KERNEL);
		if (dev->dma_parms == NULL)
			pr_err("%s:failed to allocate memory for dma parms\n",
				__func__);
	}
	rc = msm_audio_ion_reg_chrdev(&msm_audio_ion_data);
	if (rc)
		pr_err("%s register char driver failed %d\n", __func__, rc);
	return rc;
}

static int msm_audio_ion_remove(struct platform_device *pdev)
{
	msm_audio_ion_data.smmu_enabled = 0;
	msm_audio_ion_data.device_status = 0;
	msm_audio_ion_unreg_chrdev(&msm_audio_ion_data);
	return 0;
}

static struct platform_driver msm_audio_ion_driver = {
	.driver = {
		.name = "msm-audio-ion",
		.owner = THIS_MODULE,
		.of_match_table = msm_audio_ion_dt_match,
	},
	.probe = msm_audio_ion_probe,
	.remove = msm_audio_ion_remove,
};

int __init msm_audio_ion_init(void)
{
	return platform_driver_register(&msm_audio_ion_driver);
}

void msm_audio_ion_exit(void)
{
	platform_driver_unregister(&msm_audio_ion_driver);
}
module_init(msm_audio_ion_init);

MODULE_DESCRIPTION("MSM Audio ION module");
MODULE_LICENSE("GPL v2");
