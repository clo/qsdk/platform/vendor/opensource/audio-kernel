/* Copyright (c) 2011-2017, 2019 The Linux Foundation. All rights reserved.
 * Copyright (c) 2018, Linaro Limited
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/spinlock.h>
#include <linux/idr.h>
#include <linux/slab.h>
#include <linux/of_device.h>
#include <linux/workqueue.h>
#include <linux/of.h>
#include <soc/qcom/glink.h>
#include <ipc/gpr-lite.h>

#define GPR_MAX_BUF			512
#define GPR_DEFAULT_NUM_OF_INTENTS 20
#define GPR_MAXIMUM_NUM_OF_RETRIES 2

struct gpr_tx_buf {
	char buf[GPR_MAX_BUF];
};

struct link_state {
	uint32_t dest;
	void *handle;
	enum glink_link_state link_state;
	wait_queue_head_t ls_wait;
};

static struct link_state link_state;

struct gpr_add_device_work {
	struct gpr *gpr;
	struct work_struct work;
};

struct gpr {
	struct device *dev;
	spinlock_t svcs_lock;
	struct idr svcs_idr;
	int dest_domain_id;
	struct gpr_add_device_work gpr_add_dev_work;
	void *handle;
	unsigned int channel_state;
	wait_queue_head_t wait;
	bool if_remote_intent_ready;
};

/**
 * gpr_send_pkt() - Send a gpr message from gpr device
 *
 * @adev: Pointer to previously registered gpr device.
 * @pkt: Pointer to gpr packet to send
 *
 * Return: Will be an negative on packet size on success.
 */
int gpr_send_pkt(struct gpr_device *adev, struct gpr_pkt *pkt)
{
	struct gpr *gpr = dev_get_drvdata(adev->dev.parent);
	struct gpr_hdr *hdr;
	unsigned long flags;
	uint32_t pkt_size;
	int ret = 0, retries = 0;

	spin_lock_irqsave(&adev->lock, flags);

	hdr = &pkt->hdr;
	pkt_size = GPR_PKT_GET_PACKET_BYTE_SIZE(hdr->header);

	dev_dbg(gpr->dev, "SVC_ID %d %s packet size %d\n", adev->svc_id,
		__func__, pkt_size);
	do {
		if (ret == -EAGAIN)
			udelay(50);

		ret = glink_tx(gpr->handle, pkt, pkt, pkt_size,
			GLINK_TX_ATOMIC);
		if (ret)
			pr_err("%s: glink_tx failed, ret[%d]\n", __func__, ret);
		else
			ret = pkt_size;
	} while (ret == -EAGAIN && retries++ < GPR_MAXIMUM_NUM_OF_RETRIES);

	if (ret < 0) {
		pr_err("%s: Unable to send packet, ret:%d\n", __func__, ret);
	}

	spin_unlock_irqrestore(&adev->lock, flags);

	return ret;
}
EXPORT_SYMBOL_GPL(gpr_send_pkt);

static void gpr_dev_release(struct device *dev)
{
	struct gpr_device *adev = to_gpr_device(dev);

	kfree(adev);
}

static int gpr_callback(struct gpr *gpr, void *buf,
				  int len, void *priv, u32 addr)
{
	uint16_t hdr_size, pkt_size, svc_id;
	//uint16_t ver;
	struct gpr_device *svc = NULL;
	struct gpr_driver *adrv = NULL;
	struct gpr_hdr *hdr;
	unsigned long flags;
	//uint32_t opcode_type;

	if (len <= GPR_HDR_SIZE) {
		dev_err(gpr->dev, "GPR: Improper gpr pkt received:%p %d\n",
			buf, len);
		return -EINVAL;
	}

	hdr = buf;

	hdr_size = GPR_PKT_GET_HEADER_BYTE_SIZE(hdr->header);
	if (hdr_size < GPR_HDR_SIZE) {
		dev_err(gpr->dev, "GPR: Wrong hdr size:%d\n", hdr_size);
		return -EINVAL;
	}

	pkt_size = GPR_PKT_GET_PACKET_BYTE_SIZE(hdr->header);
	dev_dbg(gpr->dev,"Header %x", hdr->header);

	if (pkt_size < GPR_HDR_SIZE || pkt_size != len) {
		dev_err(gpr->dev, "GPR: Wrong packet size\n");
		return -EINVAL;
	}

	dev_dbg(gpr->dev, "%s: dst_port %x hdr_size %d pkt_size %d\n",__func__ , hdr->dst_port, hdr_size, pkt_size);

	svc_id = hdr->dst_port;
	spin_lock_irqsave(&gpr->svcs_lock, flags);
	svc = idr_find(&gpr->svcs_idr, svc_id);
	if (svc && svc->dev.driver) {
		adrv = to_gpr_driver(svc->dev.driver);
	} else {
		/*Does not match any SVC ID hence would be routed to audio passthrough*/
		svc = idr_find(&gpr->svcs_idr, GPR_SVC_MAX);
		if (svc && svc->dev.driver)
			adrv = to_gpr_driver(svc->dev.driver);
	}
	spin_unlock_irqrestore(&gpr->svcs_lock, flags);

	if (!adrv) {
		dev_err(gpr->dev, "GPR: service is not registered\n");
		return -EINVAL;
	}

	/*
	 * NOTE: hdr_size is not same as GPR_HDR_SIZE as remote can include
	 * optional headers in to gpr_hdr which should be ignored
	 */

	adrv->callback(svc, buf);

	return 0;
}

static int gpr_device_match(struct device *dev, struct device_driver *drv)
{
	struct gpr_device *adev = to_gpr_device(dev);
	struct gpr_driver *adrv = to_gpr_driver(drv);
	const struct gpr_device_id *id = adrv->id_table;

	/* Attempt an OF style match first */
	if (of_driver_match_device(dev, drv))
		return 1;

	if (!id)
		return 0;

	while (id->domain_id != 0 || id->svc_id != 0) {
		if (id->domain_id == adev->domain_id &&
		    id->svc_id == adev->svc_id)
			return 1;
		id++;
	}

	return 0;
}

static int gpr_device_probe(struct device *dev)
{
	struct gpr_device *adev = to_gpr_device(dev);
	struct gpr_driver *adrv = to_gpr_driver(dev->driver);

	return adrv->probe(adev);
}

static int gpr_device_remove(struct device *dev)
{
	struct gpr_device *adev = to_gpr_device(dev);
	struct gpr_driver *adrv;
	struct gpr *gpr = dev_get_drvdata(adev->dev.parent);

	if (dev->driver) {
		adrv = to_gpr_driver(dev->driver);
		if (adrv->remove)
			adrv->remove(adev);
		spin_lock(&gpr->svcs_lock);
		idr_remove(&gpr->svcs_idr, adev->svc_id);
		spin_unlock(&gpr->svcs_lock);
	}

	return 0;
}

static int gpr_uevent(struct device *dev, struct kobj_uevent_env *env)
{
	struct gpr_device *adev = to_gpr_device(dev);
	int ret;

	ret = of_device_uevent_modalias(dev, env);
	if (ret != -ENODEV)
		return ret;

	return add_uevent_var(env, "MODALIAS=gpr:%s", adev->name);
}

struct bus_type gprbus = {
	.name		= "gprbus",
	.match		= gpr_device_match,
	.probe		= gpr_device_probe,
	.uevent		= gpr_uevent,
	.remove		= gpr_device_remove,
};
EXPORT_SYMBOL_GPL(gprbus);

static int gpr_add_device(struct device *dev, struct device_node *np,
			  const struct gpr_device_id *id)
{
	struct gpr *gpr = dev_get_drvdata(dev);
	struct gpr_device *adev = NULL;
	int ret;

	adev = kzalloc(sizeof(*adev), GFP_KERNEL);
	if (!adev)
		return -ENOMEM;

	spin_lock_init(&adev->lock);

	adev->svc_id = id->svc_id;
	adev->domain_id = id->domain_id;
	adev->version = id->svc_version;
	if (np)
		strscpy(adev->name, np->name, GPR_NAME_SIZE);
	else
		strscpy(adev->name, id->name, GPR_NAME_SIZE);

	dev_set_name(&adev->dev, "gprsvc:%s:%x:%x", adev->name,
		     id->domain_id, id->svc_id);

	adev->dev.bus = &gprbus;
	adev->dev.parent = dev;
	adev->dev.of_node = np;
	adev->dev.release = gpr_dev_release;
	adev->dev.driver = NULL;

	spin_lock(&gpr->svcs_lock);
	idr_alloc(&gpr->svcs_idr, adev, id->svc_id,
		  id->svc_id + 1, GFP_ATOMIC);
	spin_unlock(&gpr->svcs_lock);

	dev_info(dev, "Adding GPR dev: %s\n", dev_name(&adev->dev));

	ret = device_register(&adev->dev);
	if (ret) {
		dev_err(dev, "device_register failed: %d\n", ret);
		put_device(&adev->dev);
	}

	return ret;
}

static void of_register_gpr_devices(struct device *dev)
{
	struct gpr *gpr = dev_get_drvdata(dev);
	struct device_node *node;

	for_each_child_of_node(dev->of_node, node) {
		struct gpr_device_id id = { {0} };

		if (of_property_read_u32(node, "reg", &id.svc_id))
			continue;

		id.domain_id = gpr->dest_domain_id;

		if (gpr_add_device(dev, node, &id))
			dev_err(dev, "Failed to add gpr %d svc\n", id.svc_id);
	}
}

void gpr_tal_notify_rx(void *handle, const void *priv, const void *pkt,
		       const void *ptr, size_t size)
{
	struct gpr *gpr = (struct gpr *)priv;

	if (!gpr || !ptr) {
		pr_err("%s: Invalid gpr or ptr\n", __func__);
		return;
	}

	pr_debug("%s: Rx packet received\n", __func__);

	gpr_callback(gpr, (void *)ptr, size, (void *)priv, 0);
	glink_rx_done(handle, ptr, true);
}

static void gpr_tal_notify_tx_abort(void *handle, const void *priv,
				    const void *pkt_priv)
{
	pr_debug("%s: tx_abort received for priv:%pK\n",
		 __func__, priv);
}

void gpr_tal_notify_tx_done(void *handle, const void *priv,
			    const void *pkt_priv, const void *ptr)
{
	pr_debug("%s: tx_done received for priv:%pK\n",
		 __func__, priv);
}

bool gpr_tal_notify_rx_intent_req(void *handle, const void *priv,
				  size_t req_size)
{
	struct gpr *gpr = (struct gpr *)priv;

	if (!gpr) {
		pr_err("%s: Invalid gpr_ch\n", __func__);
		return false;
	}

	pr_err("%s: No rx intents queued, unable to receive\n", __func__);
	return false;
}

static void gpr_tal_notify_remote_rx_intent(void *handle, const void *priv,
					    size_t size)
{
	struct gpr *gpr = (struct gpr *)priv;

	if (!gpr) {
		pr_err("%s: Invalid gpr\n", __func__);
		return;
	}
	/*
	 * This is to make sure that the far end has queued at least one intent
	 * before we attmpt any IPC. A simple bool flag is used here instead of
	 * a counter, as the far end is required to guarantee intent
	 * availability for all use cases once the channel is fully opened.
	 */
	pr_debug("%s: remote queued an intent\n", __func__);
	gpr->if_remote_intent_ready = true;
	wake_up(&gpr->wait);
}

void gpr_tal_notify_state(void *handle, const void *priv, unsigned int event)
{
	struct gpr *gpr = (struct gpr *)priv;

	if (!gpr) {
		pr_err("%s: Invalid gpr_ch\n", __func__);
		return;
	}

	gpr->channel_state = event;
	pr_info("%s: Channel state[%d]\n", __func__, event);

	if (event == GLINK_CONNECTED)
		wake_up(&gpr->wait);
}

int gpr_tal_rx_intents_config(struct gpr *gpr,
			      int num_of_intents, uint32_t size)
{
	int i;
	int rc = 0;

	if (!gpr || !num_of_intents || !size) {
		pr_err("%s: Invalid parameter\n", __func__);
		return -EINVAL;
	}

	for (i = 0; i < num_of_intents; i++) {
		rc = glink_queue_rx_intent(gpr->handle, gpr, size);
		if (rc) {
			pr_err("%s: Failed to queue rx intent, iteration[%d]\n",
			       __func__, i);
			break;
		}
	}

	return rc;
}

static void gpr_add_device_callback(struct work_struct *work)
{
	struct gpr *gpr;
	struct gpr_add_device_work *gpr_add_dev_work;
	struct glink_open_config open_cfg;
	int rc = 0;

	gpr_add_dev_work = container_of(work, struct gpr_add_device_work, work);
	gpr = gpr_add_dev_work->gpr;

	//GLINK open
	memset(&open_cfg, 0, sizeof(struct glink_open_config));
	open_cfg.options = GLINK_OPT_INITIAL_XPORT;
	open_cfg.edge = "adsp";
	open_cfg.name = "to_apps";
	open_cfg.notify_rx = gpr_tal_notify_rx;
	open_cfg.notify_tx_done = gpr_tal_notify_tx_done;
	open_cfg.notify_state = gpr_tal_notify_state;
	open_cfg.notify_rx_intent_req = gpr_tal_notify_rx_intent_req;
	open_cfg.notify_remote_rx_intent = gpr_tal_notify_remote_rx_intent;
	open_cfg.notify_tx_abort = gpr_tal_notify_tx_abort;
	open_cfg.priv = gpr;
	open_cfg.transport = "smem";

	gpr->channel_state = GLINK_REMOTE_DISCONNECTED;
	gpr->handle = glink_open(&open_cfg);
	if (IS_ERR_OR_NULL(gpr->handle)) {
		pr_err("%s: glink_open failed %s\n", __func__, open_cfg.name);
		gpr->handle = NULL;
		return;
	}

	rc = wait_event_timeout(gpr->wait,
		(gpr->channel_state == GLINK_CONNECTED), 5 * HZ);
	if (rc == 0) {
		pr_err("%s: TIMEOUT for OPEN event\n", __func__);
		rc = -ETIMEDOUT;
		goto close_link;
	}

	/*
	 * Remote intent is not required for GLINK <--> SMD IPC, so this is
	 * designed not to fail the open call.
	 */
	rc = wait_event_timeout(gpr->wait,
		gpr->if_remote_intent_ready, 5 * HZ);
	if (rc == 0)
		pr_err("%s: TIMEOUT for remote intent readiness\n", __func__);

	rc = gpr_tal_rx_intents_config(gpr, GPR_DEFAULT_NUM_OF_INTENTS,
				       GPR_MAX_BUF);
	if (rc) {
		pr_err("%s: Unable to queue intents\n", __func__);
		goto close_link;
	}

	of_register_gpr_devices(gpr->dev);
	return;

close_link:
	if (rc) {
		glink_close(gpr->handle);
		gpr->handle = NULL;
	}
}


static void gpr_tal_link_state_cb(struct glink_link_state_cb_info *cb_info,
				  void *priv)
{
	struct gpr *gpr = (struct gpr *)priv;

	if (!cb_info) {
		pr_err("%s: Invalid cb_info\n", __func__);
		return;
	}

	if (!gpr) {
		pr_err("%s: Invalid gpr handle\n", __func__);
		return;
	}

	if (!strcmp(cb_info->edge, "adsp")) {
		pr_info("%s: edge[%s] link state[%d]\n", __func__,
			cb_info->edge, cb_info->link_state);
	} else {
		pr_err("%s:Unknown edge[%s]\n", __func__, cb_info->edge);
		return;
	}

	link_state.link_state = cb_info->link_state;
	if (link_state.link_state == GLINK_LINK_STATE_UP) {
		/* create worker thread to add devices */
		gpr->gpr_add_dev_work.gpr = gpr;
		INIT_WORK(&gpr->gpr_add_dev_work.work,
			gpr_add_device_callback);
		schedule_work(&gpr->gpr_add_dev_work.work);
	}
}


static struct glink_link_info lpass_link_info = {
	.transport = "smem",
	.edge = "adsp",
	.glink_link_state_notif_cb = gpr_tal_link_state_cb,
};


static int gpr_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct gpr *gpr;
	int ret;

	gpr = devm_kzalloc(dev, sizeof(*gpr), GFP_KERNEL);
	if (!gpr)
		return -ENOMEM;

	init_waitqueue_head(&link_state.ls_wait);

	link_state.link_state = GLINK_LINK_STATE_DOWN;
	link_state.handle =
		glink_register_link_state_cb(&lpass_link_info, gpr);
	if (!link_state.handle) {
		pr_err("%s: Unable to register lpass link state\n", __func__);
		return -EINVAL;
	}

	ret = of_property_read_u32(dev->of_node, "reg", &gpr->dest_domain_id);
	if (ret) {
		dev_err(dev, "GPR Domain ID not specified in DT\n");
		return ret;
	}

	dev_set_drvdata(dev, gpr);
	gpr->dev = dev;
	init_waitqueue_head(&gpr->wait);
	spin_lock_init(&gpr->svcs_lock);
	idr_init(&gpr->svcs_idr);

	return 0;
}

static int gpr_remove(struct platform_device *pdev)
{
	return 0;
}

/*
 * __gpr_driver_register() - Client driver registration with gprbus
 *
 * @drv:Client driver to be associated with client-device.
 * @owner: owning module/driver
 *
 * This API will register the client driver with the gprbus
 * It is called from the driver's module-init function.
 */
int __gpr_driver_register(struct gpr_driver *drv, struct module *owner)
{
	drv->driver.bus = &gprbus;
	drv->driver.owner = owner;

	return driver_register(&drv->driver);
}
EXPORT_SYMBOL_GPL(__gpr_driver_register);

/*
 * gpr_driver_unregister() - Undo effect of gpr_driver_register
 *
 * @drv: Client driver to be unregistered
 */
void gpr_driver_unregister(struct gpr_driver *drv)
{
	driver_unregister(&drv->driver);
}
EXPORT_SYMBOL_GPL(gpr_driver_unregister);

static const struct of_device_id gpr_of_match[] = {
	{ .compatible = "qcom,gpr"},
	{}
};
MODULE_DEVICE_TABLE(of, gpr_of_match);

static struct platform_driver gpr_driver = {
	.probe = gpr_probe,
	.remove = gpr_remove,
	.driver = {
		.name = "gpr",
		.owner = THIS_MODULE,
		.of_match_table = gpr_of_match,
	},
};

static int __init gpr_init(void)
{
	int ret;

	ret = bus_register(&gprbus);
	if (!ret)
		ret = platform_driver_register(&gpr_driver);
	else
		bus_unregister(&gprbus);

	return ret;
}

static void __exit gpr_exit(void)
{
	bus_unregister(&gprbus);
	platform_driver_unregister(&gpr_driver);
}

subsys_initcall(gpr_init);
module_exit(gpr_exit);

/* Module information */
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("QTI GPR Bus");

