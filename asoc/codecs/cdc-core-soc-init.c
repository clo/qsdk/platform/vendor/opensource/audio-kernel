/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include "audio-ext-clk-up.h"
#include "msm-cdc-pinctrl.h"

static int __init cdc_core_soc_init(void)
{
	int ret = 0;

	ret = audio_ref_clk_platform_init();
	if (ret) {
		pr_err("%s: init extclk fail: %d\n", __func__, ret);
	} else {
		ret = msm_cdc_pinctrl_drv_init();
		if (ret) {
			pr_err("%s: init cdc pinctrl fail: %d\n",
				__func__, ret);
			audio_ref_clk_platform_exit();
		}
	}
	return ret;
}
module_init(cdc_core_soc_init);

static void __exit cdc_core_soc_exit(void)
{
	msm_cdc_pinctrl_drv_exit();
	audio_ref_clk_platform_exit();
}
module_exit(cdc_core_soc_exit);

MODULE_DESCRIPTION("CODEC soc init driver");
MODULE_LICENSE("GPL v2");
