/* Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include <sound/soc.h>
#include <soc/snd_event.h>
#include "codecs/msm-cdc-pinctrl.h"
#include "codecs/wsa881x.h"
#include <dt-bindings/sound/audio-codec-port-types.h>
#include "codecs/bolero/bolero-cdc.h"
#include "codecs/bolero/wsa-macro.h"
#include "dsp/audio_prm.h"
#include "dsp/apr_audio-v2.h"

#define DRV_NAME "ipq6018-asoc-snd"

#define __CHIPSET__ "IPQ6018 "
#define MSM_DAILINK_NAME(name) (__CHIPSET__#name)

#define DEV_NAME_STR_LEN            32

#define WSA8810_NAME_1 "wsa881x.20170211"
#define WSA8810_NAME_2 "wsa881x.20170212"

struct dev_config {
	u32 sample_rate;
	u32 bit_format;
	u32 channels;
};

enum {
	PRIM_MI2S = 0,
	SEC_MI2S,
	TERT_MI2S,
	QUAT_MI2S,
	QUIN_MI2S,
	MI2S_MAX,
};

enum {
	PRIM_AUX_PCM = 0,
	SEC_AUX_PCM,
	TERT_AUX_PCM,
	QUAT_AUX_PCM,
	QUIN_AUX_PCM,
	AUX_PCM_MAX,
};

enum {
	TDM_PRI = 0,
	TDM_SEC,
	TDM_TERT,
	TDM_QUAT,
	TDM_QUIN,
	TDM_INTERFACE_MAX,
};

#define SAMPLING_RATE_8KHZ      8000
#define SAMPLING_RATE_11P025KHZ 11025
#define SAMPLING_RATE_16KHZ     16000
#define SAMPLING_RATE_22P05KHZ  22050
#define SAMPLING_RATE_32KHZ     32000
#define SAMPLING_RATE_44P1KHZ   44100
#define SAMPLING_RATE_48KHZ     48000
#define SAMPLING_RATE_88P2KHZ   88200
#define SAMPLING_RATE_96KHZ     96000
#define SAMPLING_RATE_176P4KHZ  176400
#define SAMPLING_RATE_192KHZ    192000
#define SAMPLING_RATE_352P8KHZ  352800
#define SAMPLING_RATE_384KHZ    384000

struct mi2s_conf {
	struct mutex lock;
	u32 ref_cnt;
	u32 msm_is_mi2s_master;
};
static struct mi2s_conf mi2s_intf_conf[MI2S_MAX];

static u32 mi2s_ebit_clk[MI2S_MAX] = {
	CLOCK_ID_PRI_MI2S_EBIT,
	CLOCK_ID_SEC_MI2S_EBIT,
	CLOCK_ID_TER_MI2S_EBIT,
	CLOCK_ID_QUAD_MI2S_EBIT,
	CLOCK_ID_QUI_MI2S_EBIT
};

/* Default configuration of MI2S channels */
static struct dev_config mi2s_rx_cfg[] = {
	[PRIM_MI2S] = {SAMPLING_RATE_48KHZ, SNDRV_PCM_FORMAT_S16_LE, 2},
	[SEC_MI2S]  = {SAMPLING_RATE_48KHZ, SNDRV_PCM_FORMAT_S16_LE, 2},
	[TERT_MI2S] = {SAMPLING_RATE_48KHZ, SNDRV_PCM_FORMAT_S16_LE, 2},
	[QUAT_MI2S] = {SAMPLING_RATE_48KHZ, SNDRV_PCM_FORMAT_S16_LE, 2},
	[QUIN_MI2S] = {SAMPLING_RATE_48KHZ, SNDRV_PCM_FORMAT_S16_LE, 2},
};

struct msm_wsa881x_dev_info {
	struct device_node *of_node;
	u32 index;
};

enum pinctrl_pin_state {
	STATE_DISABLE = 0, /* All pins are in sleep state */
	STATE_MI2S_ACTIVE,  /* I2S = active, TDM = sleep */
	STATE_TDM_ACTIVE,  /* I2S = sleep, TDM = active */
};

struct msm_pinctrl_info {
	struct pinctrl *pinctrl;
	struct pinctrl_state *mi2s_disable;
	struct pinctrl_state *tdm_disable;
	struct pinctrl_state *mi2s_active;
	struct pinctrl_state *tdm_active;
	enum pinctrl_pin_state curr_state;
};

struct msm_asoc_mach_data {
	struct snd_info_entry *codec_root;
	struct msm_pinctrl_info pinctrl_info;
	struct device_node *dmic_01_gpio_p; /* used by pinctrl API */
	struct device_node *dmic_23_gpio_p; /* used by pinctrl API */
	struct device_node *dmic_45_gpio_p; /* used by pinctrl API */
	struct device_node *dmic_67_gpio_p; /* used by pinctrl API */
	struct device_node *mi2s_gpio_p[MI2S_MAX]; /* used by pinctrl API */
	int dmic_01_gpio_cnt;
	int dmic_23_gpio_cnt;
	int dmic_45_gpio_cnt;
	int dmic_67_gpio_cnt;
};

static struct clk_cfg mi2s_clk[MI2S_MAX] = {
	{
		CLOCK_ID_PRI_MI2S_IBIT,
		IBIT_CLOCK_1_P536_MHZ,
		CLOCK_ATTRIBUTE_COUPLE_NO,
		CLOCK_ROOT_DEFAULT,
	},
	{
		CLOCK_ID_SEC_MI2S_IBIT,
		IBIT_CLOCK_1_P536_MHZ,
		CLOCK_ATTRIBUTE_COUPLE_NO,
		CLOCK_ROOT_DEFAULT,
	},
	{
		CLOCK_ID_TER_MI2S_IBIT,
		IBIT_CLOCK_1_P536_MHZ,
		CLOCK_ATTRIBUTE_COUPLE_NO,
		CLOCK_ROOT_DEFAULT,
	},
	{
		CLOCK_ID_QUAD_MI2S_IBIT,
		IBIT_CLOCK_1_P536_MHZ,
		CLOCK_ATTRIBUTE_COUPLE_NO,
		CLOCK_ROOT_DEFAULT,
	},
	{
		CLOCK_ID_QUI_MI2S_IBIT,
		IBIT_CLOCK_1_P536_MHZ,
		CLOCK_ATTRIBUTE_COUPLE_NO,
		CLOCK_ROOT_DEFAULT,
	},
};


static bool codec_reg_done;
static struct snd_soc_aux_dev *msm_aux_dev;
static struct snd_soc_codec_conf *msm_codec_conf;

static int msm_dmic_event(struct snd_soc_dapm_widget *w,
			  struct snd_kcontrol *kcontrol, int event)
{
	struct msm_asoc_mach_data *pdata = NULL;
	struct snd_soc_codec *codec = snd_soc_dapm_to_codec(w->dapm);
	int ret = 0;
	uint32_t dmic_idx;
	int *dmic_gpio_cnt;
	struct device_node *dmic_gpio;
	char  *wname;

	wname = strpbrk(w->name, "01234567");
	if (!wname) {
		dev_err(codec->dev, "%s: widget not found\n", __func__);
		return -EINVAL;
	}

	ret = kstrtouint(wname, 10, &dmic_idx);
	if (ret < 0) {
		dev_err(codec->dev, "%s: Invalid DMIC line on the codec\n",
			__func__);
		return -EINVAL;
	}

	pdata = snd_soc_card_get_drvdata(codec->component.card);

	switch (dmic_idx) {
	case 0:
	case 1:
		dmic_gpio_cnt = &pdata->dmic_01_gpio_cnt;
		dmic_gpio = pdata->dmic_01_gpio_p;
		break;
	case 2:
	case 3:
		dmic_gpio_cnt = &pdata->dmic_23_gpio_cnt;
		dmic_gpio = pdata->dmic_23_gpio_p;
		break;
	case 4:
	case 5:
		dmic_gpio_cnt = &pdata->dmic_45_gpio_cnt;
		dmic_gpio = pdata->dmic_45_gpio_p;
		break;
	case 6:
	case 7:
		dmic_gpio_cnt = &pdata->dmic_67_gpio_cnt;
		dmic_gpio = pdata->dmic_67_gpio_p;
		break;
	default:
		dev_err(codec->dev, "%s: Invalid DMIC Selection\n",
			__func__);
		return -EINVAL;
	}

	dev_dbg(codec->dev, "%s: event %d DMIC%d dmic_gpio_cnt %d\n",
			__func__, event, dmic_idx, *dmic_gpio_cnt);

	switch (event) {
	case SND_SOC_DAPM_PRE_PMU:
		(*dmic_gpio_cnt)++;
		if (*dmic_gpio_cnt == 1) {
			ret = msm_cdc_pinctrl_select_active_state(
						dmic_gpio);
			if (ret < 0) {
				dev_err(codec->dev, "%s: gpio set cannot be activated %sd\n",
					__func__, "dmic_gpio");
				return ret;
			}
		}
		break;
	case SND_SOC_DAPM_POST_PMD:
		(*dmic_gpio_cnt)--;
		if (*dmic_gpio_cnt == 0) {
			ret = msm_cdc_pinctrl_select_sleep_state(
					dmic_gpio);
			if (ret < 0) {
				dev_err(codec->dev, "%s: gpio set cannot be de-activated %sd\n",
					__func__, "dmic_gpio");
				return ret;
			}
		}
		break;
	default:
		dev_err(codec->dev, "%s: invalid DAPM event %d\n",
			__func__, event);
		return -EINVAL;
	}
	return 0;
}

static u32 get_mi2s_bits_per_sample(u32 bit_format)
{
	u32 bit_per_sample;

	switch (bit_format) {
	case SNDRV_PCM_FORMAT_S32_LE:
	case SNDRV_PCM_FORMAT_S24_3LE:
	case SNDRV_PCM_FORMAT_S24_LE:
		bit_per_sample = 32;
		break;
	case SNDRV_PCM_FORMAT_S16_LE:
	default:
		bit_per_sample = 16;
		break;
	}

	return bit_per_sample;
}

static void update_mi2s_clk_val(int dai_id, int stream)
{
	u32 bit_per_sample;

	if (stream == SNDRV_PCM_STREAM_PLAYBACK) {
		bit_per_sample =
		    get_mi2s_bits_per_sample(mi2s_rx_cfg[dai_id].bit_format);
		mi2s_clk[dai_id].clk_freq_in_hz =
		    mi2s_rx_cfg[dai_id].sample_rate * 2 * bit_per_sample;
		pr_err("%s:sample rate (%d); bit_per_sample = %d\n",
		__func__, mi2s_rx_cfg[dai_id].sample_rate, bit_per_sample);
	}
}

static int msm_mi2s_set_sclk(struct snd_pcm_substream *substream, bool enable)
{
	int ret = 0;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	int port_id = 0;
	int index = cpu_dai->id;

	dev_err(rtd->card->dev, "%s:index %d", __func__, index);

	if (port_id < 0) {
		dev_err(rtd->card->dev, "%s: Invalid port_id\n", __func__);
		ret = port_id;
		goto err;
	}

	if (enable) {
		update_mi2s_clk_val(index, substream->stream);
		dev_dbg(rtd->card->dev, "%s: clock rate %u\n", __func__,
			mi2s_clk[index].clk_freq_in_hz);
	}
	dev_err(rtd->card->dev, "%s: clock rate %u\n", __func__,
		mi2s_clk[index].clk_freq_in_hz);

	ret = q6prm_set_lpass_clk_cfg(&mi2s_clk[index], enable);
	if (ret < 0) {
		dev_err(rtd->card->dev,
			"%s: prm clock failed for port 0x%x , err:%d\n",
			__func__, port_id, ret);
		goto err;
	}

err:
	return ret;
}

static int msm_mi2s_snd_startup(struct snd_pcm_substream *substream)
{
	int ret = 0;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;

	int index = cpu_dai->id;
	unsigned int fmt = SND_SOC_DAIFMT_CBS_CFS;
	struct snd_soc_card *card = rtd->card;
	struct msm_asoc_mach_data *pdata = snd_soc_card_get_drvdata(card);

	dev_dbg(rtd->card->dev,
		"%s: substream = %s  stream = %d, dai name %s, dai ID %d\n",
		__func__, substream->name, substream->stream,
		cpu_dai->name, cpu_dai->id);

	if (index < PRIM_MI2S || index >= MI2S_MAX) {
		ret = -EINVAL;
		dev_err(rtd->card->dev,
			"%s: CPU DAI id (%d) out of range\n",
			__func__, cpu_dai->id);
		goto err;
	}
	/*
	 * Mutex protection in case the same MI2S
	 * interface using for both TX and RX so
	 * that the same clock won't be enable twice.
	 */
	mutex_lock(&mi2s_intf_conf[index].lock);
	if (++mi2s_intf_conf[index].ref_cnt == 1) {
		/* Check if msm needs to provide the clock to the interface */
		if (!mi2s_intf_conf[index].msm_is_mi2s_master) {
			mi2s_clk[index].clk_id = mi2s_ebit_clk[index];
			fmt = SND_SOC_DAIFMT_CBM_CFM;
		}
		ret = msm_mi2s_set_sclk(substream, true);
		if (ret < 0) {
			dev_err(rtd->card->dev,
				"%s: afe lpass clock failed to enable MI2S clock, err:%d\n",
				__func__, ret);
			goto clean_up;
		}

		if (pdata->mi2s_gpio_p[index])
			msm_cdc_pinctrl_select_active_state(
					pdata->mi2s_gpio_p[index]);
	}
clean_up:
	if (ret < 0)
		mi2s_intf_conf[index].ref_cnt--;
	mutex_unlock(&mi2s_intf_conf[index].lock);
err:
	return ret;
}

static void msm_mi2s_snd_shutdown(struct snd_pcm_substream *substream)
{
	int ret;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	int index = rtd->cpu_dai->id;
	struct snd_soc_card *card = rtd->card;
	struct msm_asoc_mach_data *pdata = snd_soc_card_get_drvdata(card);

	pr_debug("%s(): substream = %s  stream = %d\n", __func__,
		 substream->name, substream->stream);
	if (index < PRIM_MI2S || index >= MI2S_MAX) {
		pr_err("%s:invalid MI2S DAI(%d)\n", __func__, index);
		return;
	}

	mutex_lock(&mi2s_intf_conf[index].lock);
	if (--mi2s_intf_conf[index].ref_cnt == 0) {
		if (pdata->mi2s_gpio_p[index])
			msm_cdc_pinctrl_select_sleep_state(
					pdata->mi2s_gpio_p[index]);

		ret = msm_mi2s_set_sclk(substream, false);
		if (ret < 0)
			pr_err("%s:clock disable failed for MI2S (%d); ret=%d\n",
				__func__, index, ret);
	}
	mutex_unlock(&mi2s_intf_conf[index].lock);
}

static struct snd_soc_ops msm_mi2s_snd_ops = {
	.startup = msm_mi2s_snd_startup,
	.shutdown = msm_mi2s_snd_shutdown,
};

static int msm_get_tdm_mode(u32 port_id)
{
	u32 tdm_mode;

	switch (port_id) {
	case AFE_PORT_ID_PRIMARY_TDM_RX:
	case AFE_PORT_ID_PRIMARY_TDM_TX:
		tdm_mode = TDM_PRI;
		break;
	case AFE_PORT_ID_SECONDARY_TDM_RX:
	case AFE_PORT_ID_SECONDARY_TDM_TX:
		tdm_mode = TDM_SEC;
		break;
	case AFE_PORT_ID_TERTIARY_TDM_RX:
	case AFE_PORT_ID_TERTIARY_TDM_TX:
		tdm_mode = TDM_TERT;
		break;
	case AFE_PORT_ID_QUATERNARY_TDM_RX:
	case AFE_PORT_ID_QUATERNARY_TDM_TX:
		tdm_mode = TDM_QUAT;
		break;
	case AFE_PORT_ID_QUINARY_TDM_RX:
	case AFE_PORT_ID_QUINARY_TDM_TX:
		tdm_mode = TDM_QUIN;
		break;
	default:
		pr_err("%s: Invalid port id: %d\n", __func__, port_id);
		tdm_mode = -EINVAL;
	}
	return tdm_mode;
}

static int msm_tdm_snd_startup(struct snd_pcm_substream *substream)
{
	int ret = 0;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	struct snd_soc_card *card = rtd->card;
	struct msm_asoc_mach_data *pdata = snd_soc_card_get_drvdata(card);
	u32 tdm_mode = msm_get_tdm_mode(cpu_dai->id);

	if (tdm_mode >= TDM_INTERFACE_MAX) {
		ret = -EINVAL;
		pr_err("%s: Invalid TDM interface %d\n",
			__func__, ret);
		return ret;
	}

	if (pdata->mi2s_gpio_p[tdm_mode]) {
		ret = msm_cdc_pinctrl_select_active_state(
			pdata->mi2s_gpio_p[tdm_mode]);
		if (ret)
			pr_err("%s: TDM GPIO pinctrl set active failed with %d\n",
				__func__, ret);
			return ret;
	}

	return ret;
}

static void msm_tdm_snd_shutdown(struct snd_pcm_substream *substream)
{
	int ret = 0;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	struct snd_soc_card *card = rtd->card;
	struct msm_asoc_mach_data *pdata = snd_soc_card_get_drvdata(card);
	u32 tdm_mode = msm_get_tdm_mode(cpu_dai->id);

	if (tdm_mode >= TDM_INTERFACE_MAX) {
		ret = -EINVAL;
		pr_err("%s: Invalid TDM interface %d\n",
			__func__, ret);
	}

	if (pdata->mi2s_gpio_p[tdm_mode]) {
		ret = msm_cdc_pinctrl_select_sleep_state(
			pdata->mi2s_gpio_p[tdm_mode]);
		if (ret)
			pr_err("%s: TDM GPIO pinctrl set sleep failed with %d\n",
				__func__, ret);
	}
}

static struct snd_soc_ops msm_tdm_snd_ops = {
	.startup = msm_tdm_snd_startup,
	.shutdown = msm_tdm_snd_shutdown
};

static int msm_auxpcm_snd_startup(struct snd_pcm_substream *substream)
{
	int ret = 0;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	struct snd_soc_card *card = rtd->card;
	struct msm_asoc_mach_data *pdata = snd_soc_card_get_drvdata(card);
	u32 aux_mode = cpu_dai->id;

	if (aux_mode >= AUX_PCM_MAX) {
		ret = -EINVAL;
		pr_err("%s: Invalid AUXPCM interface %d\n",
			__func__, ret);
		return ret;
	}

	if (pdata->mi2s_gpio_p[aux_mode]) {
		ret = msm_cdc_pinctrl_select_active_state(
			pdata->mi2s_gpio_p[aux_mode]);
		if (ret)
			pr_err("%s: AUXPCM GPIO pinctrl set active failed with %d\n",
				__func__, ret);
			return ret;
	}

	return ret;
}

static void msm_auxpcm_snd_shutdown(struct snd_pcm_substream *substream)
{
	int ret = 0;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	struct snd_soc_card *card = rtd->card;
	struct msm_asoc_mach_data *pdata = snd_soc_card_get_drvdata(card);
	u32 aux_mode = cpu_dai->id;

	if (aux_mode >= AUX_PCM_MAX) {
		ret = -EINVAL;
		pr_err("%s: Invalid AUXPCM interface %d\n",
			__func__, ret);
	}

	if (pdata->mi2s_gpio_p[aux_mode]) {
		ret = msm_cdc_pinctrl_select_sleep_state(
			pdata->mi2s_gpio_p[aux_mode]);
		if (ret)
			pr_err("%s: AUXPCM GPIO pinctrl set sleep failed with %d\n",
				__func__, ret);
	}
}

static struct snd_soc_ops msm_auxpcm_snd_ops = {
	.startup = msm_auxpcm_snd_startup,
	.shutdown = msm_auxpcm_snd_shutdown
};

static const struct snd_soc_dapm_widget msm_int_dapm_widgets[] = {

	SND_SOC_DAPM_MIC("Digital Mic0", msm_dmic_event),
	SND_SOC_DAPM_MIC("Digital Mic1", msm_dmic_event),
	SND_SOC_DAPM_MIC("Digital Mic2", msm_dmic_event),
	SND_SOC_DAPM_MIC("Digital Mic3", msm_dmic_event),
	SND_SOC_DAPM_MIC("Digital Mic4", msm_dmic_event),
	SND_SOC_DAPM_MIC("Digital Mic5", msm_dmic_event),
	SND_SOC_DAPM_MIC("Digital Mic6", msm_dmic_event),
	SND_SOC_DAPM_MIC("Digital Mic7", msm_dmic_event),
};

static int msm_int_audrx_init(struct snd_soc_pcm_runtime *rtd)
{
	int ret = 0;
	struct snd_soc_codec *codec = rtd->codec;
	struct snd_soc_dapm_context *dapm = snd_soc_codec_get_dapm(codec);
	struct snd_card *card;
	struct snd_soc_pcm_runtime *rtd_aux = rtd->card->rtd_aux;
	struct msm_asoc_mach_data *pdata =
				snd_soc_card_get_drvdata(rtd->card);

	snd_soc_dapm_new_controls(dapm, msm_int_dapm_widgets,
				ARRAY_SIZE(msm_int_dapm_widgets));

	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic0");
	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic1");
	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic2");
	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic3");
	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic4");
	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic5");
	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic6");
	snd_soc_dapm_ignore_suspend(dapm, "Digital Mic7");

	snd_soc_dapm_ignore_suspend(dapm, "WSA_SPK1 OUT");
	snd_soc_dapm_ignore_suspend(dapm, "WSA_SPK2 OUT");
	snd_soc_dapm_ignore_suspend(dapm, "WSA AIF VI");
	snd_soc_dapm_ignore_suspend(dapm, "VIINPUT_WSA");

	snd_soc_dapm_sync(dapm);

	/*
	 * Send speaker configuration only for WSA8810.
	 * Default configuration is for WSA8815.
	 */
	dev_dbg(codec->dev, "%s: Number of aux devices: %d\n",
		__func__, rtd->card->num_aux_devs);
	if (rtd_aux && rtd_aux->component) {
		if (!strcmp(rtd_aux->component->name, WSA8810_NAME_1) ||
		    !strcmp(rtd_aux->component->name, WSA8810_NAME_2)) {
			wsa_macro_set_spkr_mode(rtd->codec,
						WSA_MACRO_SPKR_MODE_1);
			wsa_macro_set_spkr_gain_offset(rtd->codec,
					WSA_MACRO_GAIN_OFFSET_M1P5_DB);
		}
	}
	card = rtd->card->snd_card;
	if (!pdata->codec_root)
		pdata->codec_root = snd_info_create_subdir(card->module,
					"codecs", card->proc_root);
	if (!pdata->codec_root) {
		dev_dbg(codec->dev, "%s: Cannot create codecs module entry\n",
			__func__);
		ret = 0;
		goto done;
	}
	bolero_info_create_codec_entry(pdata->codec_root, codec);
done:
	return ret;
}

static void msm_release_pinctrl(struct platform_device *pdev)
{
	struct snd_soc_card *card = platform_get_drvdata(pdev);
	struct msm_asoc_mach_data *pdata = snd_soc_card_get_drvdata(card);
	struct msm_pinctrl_info *pinctrl_info = &pdata->pinctrl_info;

	if (pinctrl_info->pinctrl) {
		devm_pinctrl_put(pinctrl_info->pinctrl);
		pinctrl_info->pinctrl = NULL;
	}
}

/* Digital audio interface glue - connects codec <---> CPU */
static struct snd_soc_dai_link msm_common_dai_links[] = {
	/* FrontEnd DAI Links */
	{
		.name = MSM_DAILINK_NAME(Media1),
		.stream_name = "CODEC_DMA-LPAIF_WSA-RX-0",
		.cpu_dai_name = "snd-soc-dummy-dai",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.dpcm_playback = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.codec_dai_name = "wsa_macro_rx1",
		.codec_name = "bolero_codec",
		.init = &msm_int_audrx_init,
		.ignore_suspend = 1,
		/* this dainlink has playback support */
		.ignore_pmdown_time = 1,
	},
	{
		.name = MSM_DAILINK_NAME(Media2),
		.stream_name = "CODEC_DMA-LPAIF_WSA-RX-1",
		.cpu_dai_name = "snd-soc-dummy-dai",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.dpcm_playback = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.codec_dai_name = "wsa_macro_rx_mix",
		.codec_name = "bolero_codec",
		.ignore_suspend = 1,
		/* this dainlink has playback support */
		.ignore_pmdown_time = 1,
	},
	{
		.name = MSM_DAILINK_NAME(Media3),
		.stream_name = "CODEC_DMA-LPAIF_WSA-TX-1",
		.cpu_dai_name = "snd-soc-dummy-dai",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.dpcm_capture = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.codec_dai_name = "wsa_macro_echo",
		.codec_name = "bolero_codec",
		.ignore_suspend = 1,
	},
	{
		.name = MSM_DAILINK_NAME(Media4),
		.stream_name = "CODEC_DMA-LPAIF_RXTX-TX-3",
		.cpu_dai_name = "snd-soc-dummy-dai",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.dpcm_capture = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.codec_dai_name = "tx_macro_tx1",
		.codec_name = "bolero_codec",
		.ignore_suspend = 1,
	},
	{
		.name = MSM_DAILINK_NAME(Media5),
		.stream_name = "CODEC_DMA-LPAIF_RXTX-TX-4",
		.cpu_dai_name = "snd-soc-dummy-dai",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.dpcm_capture = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.codec_dai_name = "tx_macro_tx2",
		.codec_name = "bolero_codec",
		.ignore_suspend = 1,
	},
};

static struct snd_soc_dai_link msm_mi2s_dai_links[] = {
	{
		.name = MSM_DAILINK_NAME(Media6),
		.stream_name = "MI2S-LPAIF_AXI-RX-PRIMARY",
		.cpu_dai_name = "msm-dai-q6-mi2s.0",
		.codec_name = "msm-stub-codec.1",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.codec_dai_name = "msm-stub-rx",
		.dpcm_playback = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.ops = &msm_mi2s_snd_ops,
		.ignore_suspend = 1,
		.ignore_pmdown_time = 1,
	},
	{
		.name = MSM_DAILINK_NAME(Media7),
		.stream_name = "MI2S-LPAIF_AXI-TX-QUATERNARY",
		.cpu_dai_name = "msm-dai-q6-mi2s.3",
		.codec_name = "msm-stub-codec.1",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.codec_dai_name = "msm-stub-tx",
		.dpcm_capture = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.ops = &msm_mi2s_snd_ops,
		.ignore_suspend = 1,
	},

};

static struct snd_soc_dai_link msm_tdm_dai_links[] = {
	{
		.name = MSM_DAILINK_NAME(Media8),
		.stream_name = "TDM-LPAIF_AXI-RX-PRIMARY",
		.cpu_dai_name = "msm-dai-q6-tdm.36864",
		.codec_name = "msm-stub-codec.1",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.codec_dai_name = "msm-stub-rx",
		.dpcm_playback = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.ops = &msm_tdm_snd_ops,
		.ignore_suspend = 1,
		.ignore_pmdown_time = 1,
	},
	{
		.name = MSM_DAILINK_NAME(Media9),
		.stream_name = "TDM-LPAIF_AXI-TX-QUATERNARY",
		.cpu_dai_name = "msm-dai-q6-tdm.36913",
		.codec_name = "msm-stub-codec.1",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.codec_dai_name = "msm-stub-tx",
		.dpcm_capture = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.ops = &msm_tdm_snd_ops,
		.ignore_suspend = 1,
	},
};

static struct snd_soc_dai_link msm_auxpcm_dai_links[] = {
	/* Primary AUX PCM Backend DAI Links */
	{
		.name = MSM_DAILINK_NAME(Media10),
		.stream_name = "AUXPCM-LPAIF_AXI-RX-SECONDARY",
		.cpu_dai_name = "msm-dai-q6-auxpcm.2",
		.codec_name = "msm-stub-codec.1",
		.codec_dai_name = "msm-stub-rx",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.dpcm_playback = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.ops = &msm_auxpcm_snd_ops,
		.ignore_pmdown_time = 1,
		.ignore_suspend = 1,
	},
	{
		.name = MSM_DAILINK_NAME(Media11),
		.stream_name = "AUXPCM-LPAIF_AXI-TX-QUATERNARY",
		.cpu_dai_name = "msm-dai-q6-auxpcm.4",
		.codec_name = "msm-stub-codec.1",
		.codec_dai_name = "msm-stub-tx",
		.async_ops = ASYNC_DPCM_SND_SOC_PREPARE,
		.dpcm_capture = 1,
		.trigger = {SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST},
		.ops = &msm_auxpcm_snd_ops,
		.ignore_suspend = 1,
	},
};

static struct snd_soc_dai_link msm_ipq6018_dai_links[
			 ARRAY_SIZE(msm_common_dai_links) +
			 ARRAY_SIZE(msm_mi2s_dai_links) +
			 ARRAY_SIZE(msm_tdm_dai_links) +
			 ARRAY_SIZE(msm_auxpcm_dai_links)];

struct snd_soc_card snd_soc_card_ipq6018_msm = {
	.name		= "ipq6018-snd-card",
};

static int msm_populate_dai_link_component_of_node(
					struct snd_soc_card *card)
{
	int i, index, ret = 0;
	struct device *cdev = card->dev;
	struct snd_soc_dai_link *dai_link = card->dai_link;
	struct device_node *np;

	if (!cdev) {
		pr_err("%s: Sound card device memory NULL\n", __func__);
		return -ENODEV;
	}

	for (i = 0; i < card->num_links; i++) {
		if (dai_link[i].platform_of_node && dai_link[i].cpu_of_node)
			continue;

		/* populate platform_of_node for snd card dai links */
		if (dai_link[i].platform_name &&
		    !dai_link[i].platform_of_node) {
			index = of_property_match_string(cdev->of_node,
						"asoc-platform-names",
						dai_link[i].platform_name);
			if (index < 0) {
				pr_err("%s: No match found for platform name: %s\n",
					__func__, dai_link[i].platform_name);
				ret = index;
				goto err;
			}
			np = of_parse_phandle(cdev->of_node, "asoc-platform",
					      index);
			if (!np) {
				pr_err("%s: retrieving phandle for platform %s, index %d failed\n",
					__func__, dai_link[i].platform_name,
					index);
				ret = -ENODEV;
				goto err;
			}
			dai_link[i].platform_of_node = np;
			dai_link[i].platform_name = NULL;
		}

		/* populate cpu_of_node for snd card dai links */
		if (dai_link[i].cpu_dai_name && !dai_link[i].cpu_of_node) {
			index = of_property_match_string(cdev->of_node,
						 "asoc-cpu-names",
						 dai_link[i].cpu_dai_name);
			if (index >= 0) {
				np = of_parse_phandle(cdev->of_node, "asoc-cpu",
						index);
				if (!np) {
					pr_err("%s: retrieving phandle for cpu dai %s failed\n",
						__func__,
						dai_link[i].cpu_dai_name);
					ret = -ENODEV;
					goto err;
				}
				dai_link[i].cpu_of_node = np;
				dai_link[i].cpu_dai_name = NULL;
			}
		}

		/* populate codec_of_node for snd card dai links */
		if (dai_link[i].codec_name && !dai_link[i].codec_of_node) {
			index = of_property_match_string(cdev->of_node,
						 "asoc-codec-names",
						 dai_link[i].codec_name);
			if (index < 0)
				continue;
			np = of_parse_phandle(cdev->of_node, "asoc-codec",
					      index);
			if (!np) {
				pr_err("%s: retrieving phandle for codec %s failed\n",
					__func__, dai_link[i].codec_name);
				ret = -ENODEV;
				goto err;
			}
			dai_link[i].codec_of_node = np;
			dai_link[i].codec_name = NULL;
		}
	}

err:
	return ret;
}

static const struct of_device_id ipq6018_asoc_machine_of_match[]  = {
	{ .compatible = "qcom,ipq6018-asoc-snd",
	  .data = "codec"},
	{ .compatible = "qcom,ipq6018-asoc-snd-stub",
	  .data = "stub_codec"},
	{},
};

static struct snd_soc_card *populate_snd_card_dailinks(struct device *dev)
{
	struct snd_soc_card *card = NULL;
	struct snd_soc_dai_link *dailink;
	int total_links = 0;
	const struct of_device_id *match;
	uint32_t mi2s_audio_intf = 0, tdm_audio_intf = 0, auxpcm_audio_intf = 0;
	uint32_t rc = 0;

	match = of_match_node(ipq6018_asoc_machine_of_match, dev->of_node);
	if (!match) {
		dev_err(dev, "%s: No DT match found for sound card\n",
				__func__);
		return NULL;
	}

	card = &snd_soc_card_ipq6018_msm;

	if (!strcmp(match->data, "codec")) {
		memcpy(msm_ipq6018_dai_links + total_links,
				msm_common_dai_links,
				sizeof(msm_common_dai_links));

		total_links += ARRAY_SIZE(msm_common_dai_links);

		dev_dbg(dev, "%s(): WSA macro in bolero codec present\n",
				__func__);
	} else if (!strcmp(match->data, "stub_codec")) {
		rc = of_property_read_u32(dev->of_node, "qcom,mi2s-audio-intf",
					  &mi2s_audio_intf);
		if (rc) {
			dev_dbg(dev, "%s: No DT match MI2S audio interface\n",
				__func__);
		} else {
			if (mi2s_audio_intf) {
				dev_err(dev, "%s: match MI2S audio interface\n",
					__func__);
				memcpy(msm_ipq6018_dai_links + total_links,
				msm_mi2s_dai_links,
				sizeof(msm_mi2s_dai_links));
				total_links +=
				ARRAY_SIZE(msm_mi2s_dai_links);
			}
		}

		rc = of_property_read_u32(dev->of_node, "qcom,tdm-audio-intf",
					  &tdm_audio_intf);
		if (rc) {
			dev_dbg(dev, "%s: No DT match TDM audio interface\n",
				__func__);
		} else {
			if (tdm_audio_intf) {
				dev_err(dev, "%s: match TDM audio interface\n",
					__func__);
				memcpy(msm_ipq6018_dai_links + total_links,
				msm_tdm_dai_links,
				sizeof(msm_tdm_dai_links));
				total_links +=
				ARRAY_SIZE(msm_tdm_dai_links);
			}
		}

		rc = of_property_read_u32(dev->of_node,
				"qcom,auxpcm-audio-intf", &auxpcm_audio_intf);
		if (rc) {
			dev_dbg(dev, "%s: No DT match AUX PCM audio interface\n",
				__func__);
		} else {
			if (tdm_audio_intf) {
				dev_err(dev, "%s:match AUXPCM audio intf\n",
					__func__);
				memcpy(msm_ipq6018_dai_links + total_links,
				msm_auxpcm_dai_links,
				sizeof(msm_auxpcm_dai_links));
				total_links +=
				ARRAY_SIZE(msm_auxpcm_dai_links);
			}
		}
	}

	dailink = msm_ipq6018_dai_links;

	if (card) {
		card->dai_link = dailink;
		card->num_links = total_links;
	}

	return card;
}

static int msm_wsa881x_init(struct snd_soc_component *component)
{
	u8 spkleft_ports[WSA881X_MAX_SWR_PORTS] = {0, 1, 2, 3};
	u8 spkright_ports[WSA881X_MAX_SWR_PORTS] = {0, 1, 2, 3};
	u8 spkleft_port_types[WSA881X_MAX_SWR_PORTS] = {SPKR_L, SPKR_L_COMP,
						SPKR_L_BOOST, SPKR_L_VI};
	u8 spkright_port_types[WSA881X_MAX_SWR_PORTS] = {SPKR_R, SPKR_R_COMP,
						SPKR_R_BOOST, SPKR_R_VI};
	unsigned int ch_rate[WSA881X_MAX_SWR_PORTS] = {2400, 600, 300, 1200};
	unsigned int ch_mask[WSA881X_MAX_SWR_PORTS] = {0x1, 0xF, 0x3, 0x3};
	struct snd_soc_codec *codec = snd_soc_component_to_codec(component);
	struct msm_asoc_mach_data *pdata;
	struct snd_soc_dapm_context *dapm;
	int ret = 0;

	if (!codec) {
		pr_err("%s codec is NULL\n", __func__);
		return -EINVAL;
	}

	dapm = snd_soc_codec_get_dapm(codec);

	if (!strcmp(component->name_prefix, "SpkrLeft")) {
		dev_dbg(codec->dev, "%s: setting left ch map to codec %s\n",
			__func__, codec->component.name);
		wsa881x_set_channel_map(codec, &spkleft_ports[0],
				WSA881X_MAX_SWR_PORTS, &ch_mask[0],
				&ch_rate[0], &spkleft_port_types[0]);
		if (dapm->component) {
			snd_soc_dapm_ignore_suspend(dapm, "SpkrLeft IN");
			snd_soc_dapm_ignore_suspend(dapm, "SpkrLeft SPKR");
		}
	} else if (!strcmp(component->name_prefix, "SpkrRight")) {
		dev_dbg(codec->dev, "%s: setting right ch map to codec %s\n",
			__func__, codec->component.name);
		wsa881x_set_channel_map(codec, &spkright_ports[0],
				WSA881X_MAX_SWR_PORTS, &ch_mask[0],
				&ch_rate[0], &spkright_port_types[0]);
		if (dapm->component) {
			snd_soc_dapm_ignore_suspend(dapm, "SpkrRight IN");
			snd_soc_dapm_ignore_suspend(dapm, "SpkrRight SPKR");
		}
	} else {
		dev_err(codec->dev, "%s: wrong codec name %s\n", __func__,
			codec->component.name);
		ret = -EINVAL;
		goto err;
	}
	pdata = snd_soc_card_get_drvdata(component->card);
	if (pdata && pdata->codec_root)
		wsa881x_codec_info_create_codec_entry(pdata->codec_root,
						      codec);

err:
	return ret;
}

static int msm_init_wsa_dev(struct platform_device *pdev,
				struct snd_soc_card *card)
{
	struct device_node *wsa_of_node;
	u32 wsa_max_devs;
	u32 wsa_dev_cnt;
	int i;
	struct msm_wsa881x_dev_info *wsa881x_dev_info;
	const char *wsa_auxdev_name_prefix[1];
	char *dev_name_str = NULL;
	int found = 0;
	int ret = 0;

	/* Get maximum WSA device count for this platform */
	ret = of_property_read_u32(pdev->dev.of_node,
				   "qcom,wsa-max-devs", &wsa_max_devs);
	if (ret) {
		dev_info(&pdev->dev,
			 "%s: wsa-max-devs property missing in DT %s, ret = %d\n",
			 __func__, pdev->dev.of_node->full_name, ret);
		card->num_aux_devs = 0;
		return 0;
	}
	if (wsa_max_devs == 0) {
		dev_warn(&pdev->dev,
			 "%s: Max WSA devices is 0 for this target?\n",
			 __func__);
		card->num_aux_devs = 0;
		return 0;
	}

	/* Get count of WSA device phandles for this platform */
	wsa_dev_cnt = of_count_phandle_with_args(pdev->dev.of_node,
						 "qcom,wsa-devs", NULL);
	if (wsa_dev_cnt == -ENOENT) {
		dev_warn(&pdev->dev, "%s: No wsa device defined in DT.\n",
			 __func__);
		goto err;
	} else if (wsa_dev_cnt <= 0) {
		dev_err(&pdev->dev,
			"%s: Error reading wsa device from DT. wsa_dev_cnt = %d\n",
			__func__, wsa_dev_cnt);
		ret = -EINVAL;
		goto err;
	}

	/*
	 * Expect total phandles count to be NOT less than maximum possible
	 * WSA count. However, if it is less, then assign same value to
	 * max count as well.
	 */
	if (wsa_dev_cnt < wsa_max_devs) {
		dev_dbg(&pdev->dev,
			"%s: wsa_max_devs = %d cannot exceed wsa_dev_cnt = %d\n",
			__func__, wsa_max_devs, wsa_dev_cnt);
		wsa_max_devs = wsa_dev_cnt;
	}

	/* Make sure prefix string passed for each WSA device */
	ret = of_property_count_strings(pdev->dev.of_node,
					"qcom,wsa-aux-dev-prefix");
	if (ret != wsa_dev_cnt) {
		dev_err(&pdev->dev,
			"%s: expecting %d wsa prefix. Defined only %d in DT\n",
			__func__, wsa_dev_cnt, ret);
		ret = -EINVAL;
		goto err;
	}

	/*
	 * Alloc mem to store phandle and index info of WSA device, if already
	 * registered with ALSA core
	 */
	wsa881x_dev_info = devm_kcalloc(&pdev->dev, wsa_max_devs,
					sizeof(struct msm_wsa881x_dev_info),
					GFP_KERNEL);
	if (!wsa881x_dev_info) {
		ret = -ENOMEM;
		goto err;
	}

	/*
	 * search and check whether all WSA devices are already
	 * registered with ALSA core or not. If found a node, store
	 * the node and the index in a local array of struct for later
	 * use.
	 */
	for (i = 0; i < wsa_dev_cnt; i++) {
		wsa_of_node = of_parse_phandle(pdev->dev.of_node,
					    "qcom,wsa-devs", i);
		if (unlikely(!wsa_of_node)) {
			/* we should not be here */
			dev_err(&pdev->dev,
				"%s: wsa dev node is not present\n",
				__func__);
			ret = -EINVAL;
			goto err_free_dev_info;
		}
		if (soc_find_component(wsa_of_node, NULL)) {
			/* WSA device registered with ALSA core */
			wsa881x_dev_info[found].of_node = wsa_of_node;
			wsa881x_dev_info[found].index = i;
			found++;
			if (found == wsa_max_devs)
				break;
		}
	}

	if (found < wsa_max_devs) {
		dev_err(&pdev->dev,
			"%s: failed to find %d components. Found only %d\n",
			__func__, wsa_max_devs, found);
		return -EPROBE_DEFER;
	}
	dev_info(&pdev->dev,
		"%s: found %d wsa881x devices registered with ALSA core\n",
		__func__, found);

	card->num_aux_devs = wsa_max_devs;
	card->num_configs = wsa_max_devs;

	/* Alloc array of AUX devs struct */
	msm_aux_dev = devm_kcalloc(&pdev->dev, card->num_aux_devs,
				       sizeof(struct snd_soc_aux_dev),
				       GFP_KERNEL);
	if (!msm_aux_dev) {
		ret = -ENOMEM;
		goto err_free_dev_info;
	}

	/* Alloc array of codec conf struct */
	msm_codec_conf = devm_kcalloc(&pdev->dev, card->num_aux_devs,
					  sizeof(struct snd_soc_codec_conf),
					  GFP_KERNEL);
	if (!msm_codec_conf) {
		ret = -ENOMEM;
		goto err_free_aux_dev;
	}

	for (i = 0; i < card->num_aux_devs; i++) {
		dev_name_str = devm_kzalloc(&pdev->dev, DEV_NAME_STR_LEN,
					    GFP_KERNEL);
		if (!dev_name_str) {
			ret = -ENOMEM;
			goto err_free_cdc_conf;
		}

		ret = of_property_read_string_index(pdev->dev.of_node,
						    "qcom,wsa-aux-dev-prefix",
						    wsa881x_dev_info[i].index,
						    wsa_auxdev_name_prefix);
		if (ret) {
			dev_err(&pdev->dev,
				"%s: failed to read wsa aux dev prefix, ret = %d\n",
				__func__, ret);
			ret = -EINVAL;
			goto err_free_dev_name_str;
		}

		snprintf(dev_name_str, strlen("wsa881x.%d"), "wsa881x.%d", i);
		msm_aux_dev[i].name = dev_name_str;
		msm_aux_dev[i].codec_name = NULL;
		msm_aux_dev[i].codec_of_node =
					wsa881x_dev_info[i].of_node;
		msm_aux_dev[i].init = msm_wsa881x_init;
		msm_codec_conf[i].dev_name = NULL;
		msm_codec_conf[i].name_prefix = wsa_auxdev_name_prefix[0];
		msm_codec_conf[i].of_node =
				wsa881x_dev_info[i].of_node;
	}
	card->codec_conf = msm_codec_conf;
	card->aux_dev = msm_aux_dev;

	return 0;

err_free_dev_name_str:
	devm_kfree(&pdev->dev, dev_name_str);
err_free_cdc_conf:
	devm_kfree(&pdev->dev, msm_codec_conf);
err_free_aux_dev:
	devm_kfree(&pdev->dev, msm_aux_dev);
err_free_dev_info:
	devm_kfree(&pdev->dev, wsa881x_dev_info);
err:
	return ret;
}

static int ipq6018_ssr_enable(struct device *dev, void *data)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct snd_soc_card *card = platform_get_drvdata(pdev);
	int ret = 0;

	if (!card) {
		dev_err(dev, "%s: card is NULL\n", __func__);
		ret = -EINVAL;
		return ret;
	}

	snd_soc_card_change_online_state(card, 1);
	dev_dbg(dev, "%s: setting snd_card to ONLINE\n", __func__);

	return ret;
}

static void ipq6018_ssr_disable(struct device *dev, void *data)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct snd_soc_card *card = platform_get_drvdata(pdev);

	if (!card) {
		dev_err(dev, "%s: card is NULL\n", __func__);
		return;
	}

	dev_dbg(dev, "%s: setting snd_card to OFFLINE\n", __func__);
	snd_soc_card_change_online_state(card, 0);
}

static const struct snd_event_ops ipq6018_ssr_ops = {
	.enable = ipq6018_ssr_enable,
	.disable = ipq6018_ssr_disable,
};

static int msm_audio_ssr_compare(struct device *dev, void *data)
{
	struct device_node *node = data;

	dev_dbg(dev, "%s: dev->of_node = 0x%p, node = 0x%p\n",
		__func__, dev->of_node, node);
	return (dev->of_node && dev->of_node == node);
}

static int msm_audio_ssr_register(struct device *dev)
{
	struct device_node *np = dev->of_node;
	struct snd_event_clients *ssr_clients = NULL;
	struct device_node *node;
	int ret;
	int i;

	for (i = 0; ; i++) {
		node = of_parse_phandle(np, "qcom,msm_audio_ssr_devs", i);
		if (!node)
			break;
		snd_event_mstr_add_client(&ssr_clients,
					msm_audio_ssr_compare, node);
	}

	ret = snd_event_master_register(dev, &ipq6018_ssr_ops,
					ssr_clients, NULL);
	if (!ret)
		snd_event_notify(dev, SND_EVENT_UP);

	return ret;
}

static void msm_mi2s_init(struct platform_device *pdev)
{
	int count;
	u32 mi2s_master_slave[MI2S_MAX];
	int ret;

	for (count = 0; count < MI2S_MAX; count++) {
		mutex_init(&mi2s_intf_conf[count].lock);
		mi2s_intf_conf[count].ref_cnt = 0;
	}

	ret = of_property_read_u32_array(pdev->dev.of_node,
			"qcom,msm-mi2s-master",
			mi2s_master_slave, MI2S_MAX);
	if (ret) {
		dev_dbg(&pdev->dev, "%s: no qcom,msm-mi2s-master in DT node\n",
			__func__);
	} else {
		for (count = 0; count < MI2S_MAX; count++) {
			mi2s_intf_conf[count].msm_is_mi2s_master =
				mi2s_master_slave[count];
		}
	}
}

static void msm_i2s_auxpcm_deinit(void)
{
	int count;

	for (count = 0; count < MI2S_MAX; count++) {
		mutex_destroy(&mi2s_intf_conf[count].lock);
		mi2s_intf_conf[count].ref_cnt = 0;
		mi2s_intf_conf[count].msm_is_mi2s_master = 0;
	}
}

static int msm_asoc_machine_probe(struct platform_device *pdev)
{
	struct snd_soc_card *card;
	struct msm_asoc_mach_data *pdata;
	int ret;

	pr_debug("%s:\n", __func__);
	if (!pdev->dev.of_node) {
		dev_err(&pdev->dev, "No platform supplied from device tree\n");
		return -EINVAL;
	}

	pdata = devm_kzalloc(&pdev->dev,
			sizeof(struct msm_asoc_mach_data), GFP_KERNEL);
	if (!pdata)
		return -ENOMEM;

	card = populate_snd_card_dailinks(&pdev->dev);
	if (!card) {
		dev_err(&pdev->dev, "%s: Card uninitialized\n", __func__);
		ret = -EINVAL;
		return ret;
	}
	card->dev = &pdev->dev;
	platform_set_drvdata(pdev, card);
	snd_soc_card_set_drvdata(card, pdata);

	ret = snd_soc_of_parse_card_name(card, "qcom,model");
	if (ret) {
		dev_err(&pdev->dev, "parse card name failed, err:%d\n",
			ret);
		return ret;
	}

	ret = snd_soc_of_parse_audio_routing(card, "qcom,audio-routing");
	if (ret) {
		dev_err(&pdev->dev, "parse audio routing failed, err:%d\n",
			ret);
		return ret;
	}

	ret = msm_populate_dai_link_component_of_node(card);
	if (ret) {
		ret = -EPROBE_DEFER;
		return ret;
	}

	ret = msm_init_wsa_dev(pdev, card);
	if (ret)
		return ret;

	pdata->dmic_01_gpio_p = of_parse_phandle(pdev->dev.of_node,
					"qcom,cdc-dmic01-gpios", 0);
	pdata->dmic_23_gpio_p = of_parse_phandle(pdev->dev.of_node,
					"qcom,cdc-dmic23-gpios", 0);
	pdata->dmic_45_gpio_p = of_parse_phandle(pdev->dev.of_node,
					"qcom,cdc-dmic45-gpios", 0);
	pdata->dmic_67_gpio_p = of_parse_phandle(pdev->dev.of_node,
					"qcom,cdc-dmic67-gpios", 0);

	pdata->mi2s_gpio_p[PRIM_MI2S] = of_parse_phandle(pdev->dev.of_node,
					"qcom,pri-mi2s-gpios", 0);
	pdata->mi2s_gpio_p[SEC_MI2S] = of_parse_phandle(pdev->dev.of_node,
					"qcom,sec-mi2s-gpios", 0);
	pdata->mi2s_gpio_p[TERT_MI2S] = of_parse_phandle(pdev->dev.of_node,
					"qcom,tert-mi2s-gpios", 0);
	pdata->mi2s_gpio_p[QUAT_MI2S] = of_parse_phandle(pdev->dev.of_node,
					"qcom,quat-mi2s-gpios", 0);
	pdata->mi2s_gpio_p[QUIN_MI2S] = of_parse_phandle(pdev->dev.of_node,
					"qcom,quin-mi2s-gpios", 0);

	ret = devm_snd_soc_register_card(&pdev->dev, card);
	if (ret == -EPROBE_DEFER) {
		if (codec_reg_done)
			ret = -EINVAL;
		return ret;
	} else if (ret) {
		dev_err(&pdev->dev, "snd_soc_register_card failed (%d)\n",
			ret);
		return ret;
	}
	dev_info(&pdev->dev, "Sound card %s registered\n", card->name);

	ret = msm_audio_ssr_register(&pdev->dev);
	if (ret)
		pr_err("%s: Registration with SND event FWK failed ret = %d\n",
			__func__, ret);

	msm_mi2s_init(pdev);

	return 0;
}

static int msm_asoc_machine_remove(struct platform_device *pdev)
{
	snd_event_master_deregister(&pdev->dev);
	msm_i2s_auxpcm_deinit();
	msm_release_pinctrl(pdev);
	return 0;
}

static struct platform_driver ipq6018_asoc_machine_driver = {
	.driver = {
		.name = DRV_NAME,
		.owner = THIS_MODULE,
		.pm = &snd_soc_pm_ops,
		.of_match_table = ipq6018_asoc_machine_of_match,
	},
	.probe = msm_asoc_machine_probe,
	.remove = msm_asoc_machine_remove,
};
module_platform_driver(ipq6018_asoc_machine_driver);

MODULE_DESCRIPTION("ALSA SoC IPQ6018 Machine driver");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:" DRV_NAME);
MODULE_DEVICE_TABLE(of, ipq6018_asoc_machine_of_match);
