/*
 * Copyright (c) 2013-2015, 2017-2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef _LINUX_MSM_AUDIO_ION_H
#define _LINUX_MSM_AUDIO_ION_H
#include <dsp/q6asm-v2.h>
#include <sound/pcm.h>
#include <linux/msm_ion.h>

int msm_audio_ion_map(const char *name, struct ion_client **client,
			struct ion_handle **handle, int fd,
			unsigned long *ionflag, size_t bufsz,
			u64 *paddr, size_t *pa_len, void **vaddr);
int msm_audio_get_phy_addr(int fd, uint64_t *paddr);
void msm_audio_ion_crash_handler(void);
#endif /* _LINUX_MSM_AUDIO_ION_H */
